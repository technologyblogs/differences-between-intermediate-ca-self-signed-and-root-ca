In order to understand the software construction in a computer, you need to know about the various certificates it holds. For this, you should know the differences between an Intermediate CA Certificate, a Self-Signed Certificate, and a Root CA Certificate. We’ll be discussing these so that you can look at some example of let's encrypt certificate below:[https://pwdconstruction.co.uk/](https://pwdconstruction.co.uk)

**Self-Signed Certificates**

These are documents when one signs both the Issued To and Issued By fields themselves. With such certificates, there is usually a warning included stating that the document is not really trustworthy. In order to correct this problem, we look at certificate stores. 
These certificates are quite handy as they can be made quickly and easily. The tools for churning them out are also easily available and have quite a few choices within their range. Since one cannot always pay what it takes to get a certificate from the proper authorities, some people may take this shortcut. It’s not uncommon for graphic designers to test out their programs with the help of a self-signed certificate. 

**Root CA Certificate**

This is merely a CA certificate which is self-signed. However, it does give some sort of connection to a certified authority. This authority is the CA or Certificate Authority. The Root CA certificate is mostly used in a different manner than other kinds of certificates. 

On Windows, the Trusted Root Certification Authorities would show the Root CA certificates too. This would also include the public root CA’s that come pre-installed with Windows. They would be updated when Windows updates itself and would be relatively fewer in number. 

**Intermediate CA Certificate**

This sort of CA certificate is not a self-signed one. While it may be used for the same purpose as a CA certificate, it could also be used for several other purposes. When trying to identify whether a certificate is an Intermediate CA one or a Root CA one, you only need to look at the Issued To and Issued By Fields. If they are same, you have a Root CA certificate or else it’s an Intermediate CA certificate.
One may not always be required to have an Intermediate CA certificate, but you would have to get one eventually if you want to move forward. As the PKI requirement goes up, you would need a higher number of CA’s. Since a CA is a sever machine performing computational tasks, it would need several machines to help it out. These machines need replicas if you want them to function correctly. Too many Root CA’s would compromise the security and integrity of the system. This is why Intermediate CA needs to come into play. The Root CA’s could delegate their work to one mutual Intermediate CA.
If you are working with a Windows operating system, you can find these certificates in the Intermediate Certification Authorities Store. There would be a considerable higher number of certificates in this location than the Trusted Root Certification Authorities store.
